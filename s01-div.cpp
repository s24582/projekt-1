#include < iostream > // for std :: cout , std :: cerr , and std :: cin
#include <string>

auto main(int argc, char* argv[]) -> int
{
    if (argc > 2) {
        auto const a = std::stof(argv[1]);
        auto const b = std::stof(argv[2]);
        if (b != 0) {
            std::cout << (a / b) << '\n';
        } else {
            std::cout << "Nie można dzielic przez 0 \n";
        }
    } else {
        std::cout << "Podaj poprawne wartosci \n";
    }
    return 0;
}