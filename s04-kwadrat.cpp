#include <iostream>
#include <time.h>
#include <string>
#include <stack>
#include <stdio.h>

struct Prostokat {
	float x, y, scale_x, scale_y;
	auto area() const -> float;
	auto draw() const -> void;
};

auto Prostokat::area() const -> float {
	std::cout << scale_x * scale_y << '\n';
	return 0;
}

auto Prostokat::draw() const -> void {

	for (int i = 0; i < scale_x; i++)
		std::cout << "-";
	std::cout << '\n';
	for (int i = 0; i < scale_y; i++) {
		std::cout << "|";
		for (int i = 0; i < scale_x-2; i++)
			std::cout << " ";
		std::cout << "|\n";
	}

	for (int i = 0; i < scale_x; i++)
		std::cout << "-";
	std::cout << '\n';
}


int main(int argc, char* argv[])
{
	float x = std::atof(argv[1]);
	float y = std::atof(argv[2]);
	float scale_x = std::atof(argv[3]);
	float scale_y = std::atof(argv[4]);

	Prostokat tomek = { x,y,scale_x, scale_y };
	tomek.area();
	tomek.draw();

	return 0;
}