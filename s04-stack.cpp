#include <iostream>
#include <time.h>
#include <string.h>
#include <stack>


int main()
{
	std::string str;
	std::cin >> str;
	std::stack<char> stos;

	bool poprawny = true;

	for (int i = 0; i < str.length(); i++) {
		poprawny = true;
		if (str[i] == '(' || str[i] == '[' || str[i] == '{')
			stos.push(str[i]);

		else if (str[i] == ')') {
			if (stos.top() != '(') {
				poprawny = false;
				break;
			}
		}
		else if (str[i] == '}') {
			if (stos.top() != '{') {
				poprawny = false;
				break;
			}
		}
		else if (str[i] == ']') {
			if (stos.top() != '[') {
				poprawny = false;
				break;
			}
		}
			
		if (!stos.empty() && (str[i] == ')' || str[i] == ']' || str[i] == '}'))
			stos.pop();
	}

	if (!stos.empty())
		poprawny = false;

	if (poprawny)
		std::cout << "OK\n";
	else
		std::cout << "ERROR\n";

	return 0;
}