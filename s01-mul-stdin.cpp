#include <iostream>
#include <string>

auto ask_user_for_integer (std::string prompt) -> int
{
    std::cout<<prompt<<"podaj liczbe ";
    auto c = std::string{};
    std::getline(std::cin, c);
    return std::stoi(c);
}


    auto main () -> int
{
	auto const a = ask_user_for_integer ("a = ");
	auto const b = ask_user_for_integer ("b = ");
	std::cout << (a * b ) <<"\n";
	return 0;
	
}
